;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'nonen
  authors "Dima Akater"
  first-publication-year-as-string "2023"
  org-files-in-order '("nonen-core"
                       "nonen-ru"
                       "nonen"
                       )
  ;; org-files-for-testing-in-order '("nonen-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
